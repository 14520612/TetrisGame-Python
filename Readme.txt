----- How To Run ------
1. Install Python 2.7.9 & the Pygame library (for graphic)
	- A guide could be found here: http://introcs.cs.princeton.edu/python/windows/
2. For Window users:
	- Go to the "Source Code" folder & double click on the "Tetris.py" file
		(by default, it will run using the python program, if not, you can change your default apps to open & run it)
	- Or you can open the Command Prompt, move to the "Source Code" folder and run the game using the command: "python Tetris.py"


----- Control Keys ------
       Down - Drop stone faster
 Left/Right - Move stone
         Up - Rotate Stone clockwise
     Escape - Quit game
          P - Pause game
     Return - Instant drop

---- About The Game ----
This game is divided into 4 modes:
	- Classical : A normal Tetris Game can be found anywhere
	- Multiplayer: 2 player can play Tetris which each others, who reach a certain score first wins
	- Ghost Mode: When you earn points, the blocks will have the same color of the backgrounds, so it will be a lot harder to play
	- Arcade Mode: Just like Classical mode but have 2 more functions: Ultimate arrows that can destroy any columns (appear randomly (10 %)) & A block will drop very fast at a random location every 10 turns

---- About Us ----
 14520560: Nguyễn Việt Nam
 14520612: Trần Trí Nguyên
 14520837: Hoàng Bá Thanh

 -- Gitlab link: https://gitlab.com/14520612/TetrisGame-Python

 Enjoy the game.
