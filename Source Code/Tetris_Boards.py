def check_collision(board, shape, offset):
	off_x, off_y = offset
	for cy, row in enumerate(shape):
		for cx, cell in enumerate(row):
			try:
				if cell and board[ cy + off_y ][ cx + off_x ]:
					return True
			except IndexError:
				return True
	return False

def remove_row(board, row, num_col):
	del board[row]
	return [[0 for i in xrange(num_col)]] + board

def remove_col(board, col):
        for i in xrange(len(board)):
                board[i][col] = 0
        
        

def join_matrixes(mat1, mat2, mat2_off):
	off_x, off_y = mat2_off
	for cy, row in enumerate(mat2):
		for cx, val in enumerate(row):
			mat1[cy+off_y-1	][cx+off_x] += val
	return mat1

def new_board(rows, cols):
	board = [ [ 0 for x in xrange(cols) ]
			for y in xrange(rows) ]
	board += [[ 1 for x in xrange(cols)]]
	return board

def set_background(rows, cols):
	return [[ 8 if x % 2 == y % 2 else 0 for x in xrange(cols)] for y in xrange(rows)]
