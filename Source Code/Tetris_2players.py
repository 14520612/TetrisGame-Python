import pygame, sys
from Tetris_Config import *
from Tetris_Blocks import *
from Tetris_Boards import *
from Tetris_Player import *
from Tetris_Classic import *

config_player1 = DefaultConfig_Player_1()
config_player2 = DefaultConfig_Player_2()

block = Blocks()
cell_size =	config.get_cell_size()
cols =		config.get_cols()
rows =		config.get_rows()
maxfps = 	config.get_maxfps()

class TetrisApp_Mode_Multiplayer(TetrisApp_Mode_1Player):
	def init_screen(self):
		# Size of a one-person screen
		self.width = cell_size * (cols + config.get_info_width()) * 2
		self.height = cell_size * rows
		self.rlim = cell_size * cols
		
		self.bground_grid = set_background(rows, cols)
		self.default_font =  pygame.font.Font(config.get_font(2), config.get_font_size())
		self.default_font_number = pygame.font.Font(config.get_font(13), config.get_font_size() + 15)
		self.screen = pygame.display.set_mode((self.width, self.height))
	
	def init_game(self):
		self.player_1 = TetrisPlayer()
		self.player_2 = TetrisPlayer()
		self.player_1.board = new_board(rows, cols)
		self.player_1.next_stone = block.get_random_tetris_shape()	# Generate a new stone (block) randomly
		self.new_stone(self.player_1)

		self.player_2.board = new_board(rows, cols)
		self.player_2.next_stone = block.get_random_tetris_shape()	# Generate a new stone (block) randomly
		self.new_stone(self.player_2)

		self.highscores_list = []
		self.highscores_list_display = ""
		self.get_highscores_list()

		self.animation_1_folder = ".\\image\\multi_2_gif\\"
		self.animation_1_counter = 1
		self.max_animation_1_frames = 30

		self.animation_2_folder = ".\\image\\multi_1_gif\\"
		self.animation_2_counter = 1
		self.max_animation_2_frames = 8

		self.animation_speed = 400

		pygame.time.set_timer(pygame.USEREVENT + 1, config.get_multiplayer_const_delay())
		pygame.time.set_timer(pygame.USEREVENT + 2, animation_speed)

		if (config_player1.is_sound()):
			self.PlayIngameTheme()
		self.PlayEndGameTheme = False

	def PlayIngameTheme(self):
		#play theme song while playing
		config.PlayThemeSong("Theme\Tetris_Arcade.mp3",1.0,-1)

	def new_stone(self, player_data):
		player_data.stone = player_data.next_stone[:]			# Set current stone = next stone
		player_data.next_stone = block.get_random_tetris_shape()	# Generate new stone
		player_data.stone_x, player_data.stone_y = self.generate_stone_location()
		
		if check_collision(player_data.board, player_data.stone, (player_data.stone_x, player_data.stone_y)):	# The newly created stone collide with the others
			self.gameover = True
			if player_data is self.player_1:
				self.player_1_gameover = True
			elif player_data is self.player_2:
				self.player_2_gameover = True

	def rotate_stone(self, player_data):
		if not self.gameover and not self.paused:
			new_stone = rotate_clockwise(player_data.stone)
			if not check_collision(player_data.board, new_stone, (player_data.stone_x, player_data.stone_y)):
				player_data.stone = new_stone

	# Update lines, level & point based on completed lines
	def add_cl_lines(self, completed_lines, player_data):
		player_data.lines += completed_lines
		player_data.score += config.get_linescores(completed_lines) * player_data.level
		if (player_data.score > config_player1.get_multiplayer_max_point()):
			if (player_data is self.player_1):
				self.player_2_gameover = True
			elif (player_data is self.player_2):
				self.player_1_gameover = True
			self.gameover = True
		if player_data.lines >= player_data.level * config.get_lines_per_level():
			player_data.level += 1

	def clear_complete_rows(self, player_data):
		cleared_rows = 0
		while True:
			for i, row in enumerate(player_data.board[:-1]):
				if 0 not in row:
					player_data.board = remove_row(player_data.board, i, cols)
					cleared_rows += 1
					break
			else:
				break
		return cleared_rows
	
	# Move the block to the left/right (delta_x = -1/+1)
	def move(self, delta_x, player_data):
		if not self.gameover and not self.paused:
			new_x = player_data.stone_x + delta_x			# Change x-coordinate of the block
			# Check for collision & contraint
			if new_x < 0:
				new_x = 0
			if new_x > cols - len(player_data.stone[0]):
				new_x = cols - len(player_data.stone[0])
			if not check_collision(player_data.board, player_data.stone, (new_x, player_data.stone_y)):
				player_data.stone_x = new_x
	
	def drop(self, player_data):
		if not self.gameover and not self.paused:
			player_data.stone_y += 1
			if check_collision(player_data.board, player_data.stone, (player_data.stone_x, player_data.stone_y)):
				player_data.board = join_matrixes(player_data.board, player_data.stone, (player_data.stone_x, player_data.stone_y))
				self.new_stone(player_data)
				cleared_rows = self.clear_complete_rows(player_data)
				self.add_cl_lines(cleared_rows, player_data)
				return True
		return False
	
	def instant_drop(self, player_data):
		if not self.gameover and not self.paused:
			while(not self.drop(player_data)):
				pass
	
	def print_info(self):
		# Print Player 1 board
		pygame.draw.line(self.screen, config_player1.get_text_color(), (config_player1.get_info_width() * cell_size - 1, 0), (config_player1.get_info_width() * cell_size - 1, self.height - 1))
		
		self.disp_msg("PLAYER 1", (cell_size * 0.5 , cell_size / 2))
		self.disp_msg("Next:", (cell_size, cell_size * 2))
		self.disp_msg("Score: ", (cell_size, cell_size * 6))
		self.disp_msg("%d\n\n" % (self.player_1.score), (cell_size * 1.5, cell_size * 7), 1)
		self.disp_msg("Level: ", (cell_size, cell_size * 9))
		self.disp_msg("%d\n" % (self.player_1.level), (cell_size * 3, cell_size * 10), 1)
		self.disp_msg("Lines: ", (cell_size , cell_size * 12))
		self.disp_msg("%d" % (self.player_1.lines), (cell_size * 3, cell_size * 13), 1)

		self.draw_matrix(self.bground_grid, (config_player1.get_info_width() ,0))
		self.draw_matrix(self.player_1.board, (config_player1.get_info_width(), 0))
		self.draw_matrix(self.player_1.stone, (config_player1.get_info_width() + self.player_1.stone_x, self.player_1.stone_y))
		self.draw_matrix(self.player_1.next_stone, (1, 3))

		# Print Player 2 board
		pygame.draw.line(self.screen, config_player1.get_text_color(), (self.width / 2 - 1, 0), (self.width / 2- 1, self.height - 1))
		pygame.draw.line(self.screen, config_player1.get_text_color(), (self.width / 2 + self.rlim + 1, 0), (self.width / 2 + self.rlim + 1, self.height - 1))

		self.disp_msg("PLAYER 2", (self.width / 2 + self.rlim + cell_size * 0.5 , cell_size / 2))
		self.disp_msg("Next:", (self.width / 2 + self.rlim + cell_size, cell_size * 2))
		self.disp_msg("Score: ", (self.width / 2 + self.rlim + cell_size, cell_size * 6))
		self.disp_msg("%d\n\n" % (self.player_2.score), (self.width / 2 + self.rlim + cell_size * 1.5, cell_size * 7), 1)
		self.disp_msg("Level: ", (self.width / 2 + self.rlim + cell_size, cell_size * 9))
		self.disp_msg("%d\n" % (self.player_2.level), (self.width / 2 + self.rlim + cell_size * 3, cell_size * 10), 1)
		self.disp_msg("Lines: ", (self.width / 2 + self.rlim + cell_size , cell_size * 12))
		self.disp_msg("%d" % (self.player_2.lines), (self.width / 2 + self.rlim + cell_size * 3, cell_size * 13), 1)

		start_player2_screen_x = cols + config.get_info_width()

		self.draw_matrix(self.bground_grid, (start_player2_screen_x, 0))
		self.draw_matrix(self.player_2.board, (start_player2_screen_x, 0))
		self.draw_matrix(self.player_2.stone, (start_player2_screen_x + self.player_2.stone_x, self.player_2.stone_y))
		self.draw_matrix(self.player_2.next_stone, (start_player2_screen_x + cols + 1, 3))

		self.update_animation()

	def update_animation(self):
		image = pygame.image.load(self.animation_1_folder + 'image (' + str(self.animation_1_counter) + ').gif')
		self.screen.blit(image, [cell_size, self.height - 265])

		image = pygame.image.load(self.animation_2_folder + 'image (' + str(self.animation_2_counter) + ').gif')
		self.screen.blit(image, [self.width - 150, self.height - 230])

		pygame.display.flip()

	def print_game_over(self):
		if (self.player_1_gameover):
			self.center_msg("Player 2 WIN\n")
		elif (self.player_2_gameover):
			self.center_msg("Player 1 WIN\n")
		else:
			self.center_msg("TIE\n")

	def start_game(self):
		if self.gameover:
			self.init_game()
			self.gameover = False
			self.player_1_gameover = False
			self.player_2_gameover = False

	def excecute_event(self, key_actions):
		for event in pygame.event.get():
			if event.type == pygame.USEREVENT + 1:
				self.drop(self.player_1)
				self.drop(self.player_2)
			elif event.type == pygame.USEREVENT + 2:
				self.animation_1_counter += 1
				if (self.animation_1_counter > self.max_animation_1_frames):
					self.animation_1_counter = 1
				self.animation_2_counter += 1
				if (self.animation_2_counter > self.max_animation_2_frames):
					self.animation_2_counter = 1
			elif event.type == pygame.QUIT:
				self.quit()
			elif event.type == pygame.KEYDOWN:
				for key in key_actions:
					if event.key == eval("pygame.K_" + key):
						key_actions[key]()

	def update_status_on_screen(self):
		if self.gameover:
			self.print_game_over()
		else:
			self.screen.fill(config.get_background_color())
			if self.paused:
				self.print_pause()
			else:
				self.print_info()
		pygame.display.update()

	def run(self):
		key_actions = {
			config_player1.get_key_quit()				:	self.quit,
			config_player1.get_key_pause()				:	self.toggle_pause,
			config_player1.get_key_start()				:	self.start_game,

			config_player1.get_key_left()					:	lambda:self.move(-1, self.player_1),
			config_player1.get_key_right()					:	lambda:self.move(+1, self.player_1),
			config_player1.get_key_down()					:	lambda:self.drop(self.player_1),
			config_player1.get_key_up()						:	lambda:self.rotate_stone(self.player_1),
			config_player1.get_key_instant_drop()			:	lambda:self.instant_drop(self.player_1),

			config_player2.get_key_left()					:	lambda:self.move(-1, self.player_2),
			config_player2.get_key_right()					:	lambda:self.move(+1, self.player_2),
			config_player2.get_key_down()					:	lambda:self.drop(self.player_2),
			config_player2.get_key_up()						:	lambda:self.rotate_stone(self.player_2),
			config_player2.get_key_instant_drop()			:	lambda:self.instant_drop(self.player_2)
		}
		
		self.gameover = False
		self.player_1_gameover = False
		self.player_2_gameover = False
		self.paused = False
		
		dont_burn_my_cpu = pygame.time.Clock()
		while 1:
			self.update_status_on_screen()
			self.excecute_event(key_actions)
			dont_burn_my_cpu.tick(maxfps)

if __name__ == '__main__':
	App = TetrisApp_Mode_Multiplayer()
	App.run()

	