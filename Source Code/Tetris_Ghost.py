import pygame, sys, os.path
from Tetris_Config import *
from Tetris_Blocks import *
from Tetris_Boards import *
from Tetris_Player import *
from Tetris_Classic import *

config = DefaultConfig_Player_2()
block = Blocks()
cell_size =	config.get_cell_size()
cols =		config.get_cols()
rows =		config.get_rows()
maxfps = 	config.get_maxfps()

class TetrisApp_Ghost_Mode(TetrisApp_Mode_1Player):
	def init_game(self):
		TetrisApp_Mode_1Player.init_game(self)

		self.animation_folder = ".\\image\\ghostmode_gif\\"
		self.animation_counter = 1
		self.animation_speed = 300
		self.max_animation_frames = 71


	def draw_invisible_matrix(self, matrix, offset):
		off_x, off_y  = offset
		for y, row in enumerate(matrix):
			for x, val in enumerate(row):
				if val:
					pygame.draw.rect(self.screen, config.get_background_color(), pygame.Rect(
																				(off_x + x) * cell_size,
																				(off_y + y) * cell_size, 
																				cell_size, cell_size),	0)

	def print_info(self):
		pygame.draw.line(self.screen, config.get_text_color(), (self.rlim + 1, 0), (self.rlim + 1, self.height - 1))

		self.disp_msg("Tetris - Ghost", (self.rlim + cell_size * 0.5 , cell_size / 2))
		self.disp_msg("Next:", ( self.rlim + cell_size, cell_size * 2))
		self.disp_msg("Score: ", (self.rlim + cell_size, cell_size * 6))
		self.disp_msg("%d\n\n" % (self.player_1.score), (self.rlim + cell_size * 1.5, cell_size * 7), 1)
		self.disp_msg("Level: ", (self.rlim + cell_size, cell_size * 9))
		self.disp_msg("%d\n" % (self.player_1.level), (self.rlim + cell_size * 3, cell_size * 10), 1)
		self.disp_msg("Lines: ", (self.rlim + cell_size , cell_size * 12))
		self.disp_msg("%d" % (self.player_1.lines), (self.rlim + cell_size * 3, cell_size * 13), 1)

		self.draw_matrix(self.bground_grid, (0,0))
		self.draw_matrix(self.player_1.board, (0,0))

		self.draw_matrix(self.player_1.next_stone, (cols + 1, 3))
		if self.player_1.score == 0:
			self.draw_matrix(self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y))
		else:
			self.draw_invisible_matrix(self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y))

		self.disp_msg(self.highscores_list_display,(self.rlim + cell_size, cell_size * 15))
		self.update_animation()
		
	def update_animation(self):
		image = pygame.image.load(self.animation_folder + 'image (' + str(self.animation_counter) + ').gif')
		self.screen.blit(image, [self.width - 175, self.height - 160])

		pygame.display.flip()

	def PlayIngameTheme(self):
		#play theme song while playing
		config.PlayThemeSong("Theme\Tetris_Ghost.mp3", 0.0, -1)

if __name__ == '__main__':
	App = TetrisApp_Ghost_Mode()
	App.run()