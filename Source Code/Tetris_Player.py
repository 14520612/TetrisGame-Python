from Tetris_Config import *

config = DefaultConfig()

class TetrisPlayer:
	def __init__(self):
		self.level = config.get_default_level()
		self.score = 0
		self.lines = 0

		# Dummy Initialization (Will update on gameplay)
		self.stone = []
		self.next_stone = []
		self.stone_x = 0
		self.stone_y = 0
		self.board = []
