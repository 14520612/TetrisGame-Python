from Tetris_2players import *
from Tetris_Classic import *
from Tetris_Ghost import *
from Tetris_Arcade import *
from Tetris_Config import *
from pygame.locals import *
import pygame

config = DefaultConfig()

loading_speed = 100
loading_gif_folder = '.\image\loading_gif\\'
background_circle_1_gif_folder = '.\\image\\background_gif\\'
background_circle_2_gif_folder = '.\\image\\choose_gif\\'

class Tetris_Menu:
	def __init__(self):
		self.width = config.get_menu_width()
		self.height = config.get_menu_height()
		self.screen = pygame.display.set_mode((self.width, self.height))
		self.screen.fill(config.get_menu_background_color())

		self.current_choice = 0
		self.is_info = False
		self.background_count = 1
		self.choose_image_count = 1

		pygame.init()
		pygame.time.set_timer(pygame.USEREVENT + 1, 100)
		if (config.is_sound()):
			config.PlayThemeSong("Theme\Game_Selector_Theme.mp3",0.0,-1)

	def display_title(self):
		tetris_font = pygame.font.Font(config.get_font(1), config.get_menu_title_font_size())
		tetris_font.set_bold(1)

		label_title = tetris_font.render(config.get_menu_title(), True, config.get_menu_text_color())
		label_title_rect = label_title.get_rect()
		label_title_rect.center = (self.width / 2, self.height / 5)
		
		self.screen.blit(label_title, label_title_rect)
		pygame.display.flip()

	def display_choices(self):
		init_menu_choices_y = self.height / 2.5
		tetris_font = pygame.font.Font(config.get_font(11), config.get_menu_normal_font_size())

		for i in xrange(config.get_menu_choices_num()):
			menu_choices_y = init_menu_choices_y + i * config.get_menu_choices_distance()
			if (self.current_choice == i):
				text_color = config.get_menu_selector_color()
			else:
				text_color = config.get_menu_text_color()
			label_choices = tetris_font.render(config.get_menu_choices(i) , True , text_color)
			label_choices_rect = label_choices.get_rect()
			label_choices_rect.center = (self.width / 2, menu_choices_y)

			self.screen.blit(label_choices, label_choices_rect)
			pygame.display.flip()

	def change_choices(self, move_step):
		if (move_step > 0):
			self.current_choice = (self.current_choice + move_step) % config.get_menu_choices_num()
		elif (move_step < 0):
			self.current_choice = self.current_choice + move_step
			while (self.current_choice < 0):
				self.current_choice = config.get_menu_choices_num() + self.current_choice

	def menu_input(self):
		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT: 
					pygame.quit()
					sys.exit()
				elif event.type == pygame.USEREVENT + 1 and not self.is_info:
					self.background_count = (self.background_count + 1) % 63
					self.choose_image_count = (self.choose_image_count + 1) % 45
					self.display_background()
					self.display_title()
				elif event.type == pygame.KEYDOWN:
					if (self.is_info):
						self.is_info = False
					elif event.key == pygame.K_DOWN:
						self.change_choices(1)
					elif event.key == pygame.K_UP:
						self.change_choices(-1)
					elif event.key == pygame.K_RETURN:
						self.run_mode()
						if (not self.is_info):
							pygame.quit()
							sys.exit()
					if (not self.is_info):
						self.display()
					
	def print_info(self):
		tetris_font = pygame.font.Font(config.get_font(6), config.get_menu_title_font_size())
		tetris_font.set_bold(1)
		font_color = config.get_menu_text_color()

		pygame.draw.rect(self.screen, config.get_menu_background_color(), (0, 0,self.width,self.height), 0)
		
		label = tetris_font.render("INFO",1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 2, self.height / 15)
		
		self.screen.blit(label, label_rect)
		
		tetris_font = pygame.font.Font(config.get_font(12), 16)
		height = self.height / 8
		
		label = tetris_font.render("1. Keyboard Settings", 1, font_color)
		self.screen.blit(label, (self.width / 50, height))

		config_p1 = DefaultConfig_Player_1()
		config_p2 = DefaultConfig_Player_2()
		height += 20
		self.__helper_show_keyboard_settings("", "Player 1", "Player 2", height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Rotate Stone", config_p1.get_key_up().upper(), config_p2.get_key_up(), height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Move Stone Left", config_p1.get_key_left().upper(), config_p2.get_key_left(), height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Move Stone Right", config_p1.get_key_right().upper(), config_p2.get_key_right(), height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Fast Drop", config_p1.get_key_down().upper(), config_p2.get_key_down(), height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Instant Drop", config_p1.get_key_instant_drop().upper(), config_p2.get_key_instant_drop(), height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Pause", config.get_key_pause().upper(), '', height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Resume", config.get_key_start().upper(), '', height, tetris_font, font_color)
		height += 20
		self.__helper_show_keyboard_settings("Quit", config.get_key_quit().upper(), '', height, tetris_font, font_color)

		height += 25
		label = tetris_font.render("2. About Game Modes", 1, font_color)
		self.screen.blit(label, (self.height / 50, height))

		for i in xrange(2):
			height += 20 
			label = tetris_font.render(config.get_menu_choices(i), 1, font_color)
			self.screen.blit(label, (self.height / 25, height))
			height += 20
			label = tetris_font.render(config.get_menu_choices_description(i), 1, font_color)
			self.screen.blit(label, (self.height / 10, height))

		height += 20 
		label = tetris_font.render(config.get_menu_choices(2), 1, font_color)
		self.screen.blit(label, (self.height / 25, height))
		height += 20
		label = tetris_font.render(config.get_menu_choices_description(2), 1, font_color)
		self.screen.blit(label, (self.height / 10, height))
		height += 20
		label = tetris_font.render(config.get_menu_choices_description(3), 1, font_color)
		self.screen.blit(label, (self.height / 10, height))
		height += 20 
		label = tetris_font.render(config.get_menu_choices(3), 1, font_color)
		self.screen.blit(label, (self.height / 25, height))
		height += 20
		label = tetris_font.render(config.get_menu_choices_description(4), 1, font_color)
		self.screen.blit(label, (self.height / 10, height))
		height += 20
		label = tetris_font.render(config.get_menu_choices_description(5), 1, font_color)
		self.screen.blit(label, (self.height / 10, height))
		height += 20
		label = tetris_font.render(config.get_menu_choices_description(6), 1, font_color)
		self.screen.blit(label, (self.height / 10, height))

		height += 25
		label = tetris_font.render("3. About Us", 1, font_color)
		self.screen.blit(label, (self.width / 50, height))
		height += 20
		label = tetris_font.render("Nguyen Viet Nam", 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 2, height)
		self.screen.blit(label, label_rect)
		height += 20
		label = tetris_font.render("Tran Tri Nguyen", 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 2, height)
		self.screen.blit(label, label_rect)
		height += 20
		label = tetris_font.render("Hoang Ba Thanh", 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 2, height)
		self.screen.blit(label, label_rect)

		label = tetris_font.render(" -- Press any key to quit --", 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 2, self.height - 20)
		self.screen.blit(label, label_rect)

		pygame.display.flip()

	def __helper_show_keyboard_settings(self, name, player_1_key, player_2_key, height, tetris_font, font_color):
		label = tetris_font.render(name, 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 5, height)
		self.screen.blit(label, label_rect)

		label = tetris_font.render(player_1_key, 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width / 2, height)
		self.screen.blit(label, label_rect)

		label = tetris_font.render(player_2_key, 1, font_color)
		label_rect = label.get_rect()
		label_rect.center = (self.width * 3 / 4, height)
		self.screen.blit(label, label_rect)
		
	def display(self):
		self.screen.fill(config.get_menu_background_color())
		self.display_background()
		self.display_title()
		self.display_choices()

	def display_background(self):
		image = pygame.image.load(background_circle_1_gif_folder + 'image (' + str(self.background_count + 1) + ').gif')
		self.screen.blit(image, [110, 10])

		pygame.draw.rect(self.screen, config.get_menu_background_color(), pygame.Rect(	self.width - 60,
																						self.height - 60, 
																						50, 50),			0)
		image = pygame.image.load(background_circle_2_gif_folder + 'image (' + str(self.choose_image_count + 1) + ').gif')
		self.screen.blit(image, [self.width - 60, self.height - 60])

		pygame.display.flip()

	def run_mode(self):
		if (self.current_choice == 4):
			self.print_info()
			self.is_info = True
			return None
		else: 
			# Loading State (Show animation & load game)
			load_image_num = 0
			pygame.time.set_timer(pygame.USEREVENT + 2, loading_speed)
			while True:
				for event in pygame.event.get():
					if event.type == pygame.QUIT: 
						pygame.quit()
						sys.exit()
					elif event.type == pygame.USEREVENT + 2:
						load_image_num += 1
						if (load_image_num >= 15):
							break
						image = pygame.image.load(loading_gif_folder + 'image (' + str(load_image_num) + ').gif')
						self.screen.blit(image, [self.width - 70, self.height - 60])
						pygame.display.flip()
				if (load_image_num >= 15):
					break
			if (self.current_choice == 0):
				app = TetrisApp_Mode_1Player()
			elif (self.current_choice == 1):
				app = TetrisApp_Mode_Multiplayer()
			elif (self.current_choice == 2):
				app = TetrisApp_Ghost_Mode()
			elif (self.current_choice == 3):
				app = TetrisApp_Mode_Arcade()
			app.run()
				

app = Tetris_Menu()
app.display()
app.menu_input()