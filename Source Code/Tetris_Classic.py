import pygame, sys, os.path, time
from Tetris_Config import *
from Tetris_Blocks import *
from Tetris_Boards import *
from Tetris_Player import *
from pygame.locals import *

config = DefaultConfig_Player_2()
block = Blocks()
cell_size =	config.get_cell_size()
cols =		config.get_cols()
rows =		config.get_rows()
maxfps = 	config.get_maxfps()

animation_folder = ".\\image\\classic_gif\\"
animation_counter = 1
animation_speed = 100
max_animation_frames = 8

class TetrisApp_Mode_1Player(object):
	def __init__(self):
		pygame.init()
		pygame.key.set_repeat(250, 25)	# Keys that are held will generate multiple KeyDown Event
		pygame.event.set_blocked(pygame.MOUSEMOTION) # Block mouse movement

		self.init_screen()
		self.init_game()

	def init_screen(self):
		# Size of a one-person screen
		self.width = cell_size * (cols + config.get_info_width())
		self.height = cell_size * rows
		self.rlim = cell_size * cols
		
		self.bground_grid = set_background(rows, cols)
		self.default_font =  pygame.font.Font(config.get_font(2), config.get_font_size())
		self.default_font_number = pygame.font.Font(config.get_font(13), config.get_font_size() + 15)
		self.screen = pygame.display.set_mode((self.width, self.height))
	
	def init_game(self):
		self.player_1 = TetrisPlayer()
		self.player_1.board = new_board(rows, cols)
		self.player_1.next_stone = block.get_random_tetris_shape()	# Generate a new stone (block) randomly

		self.new_stone()

		self.highscores_list = []
		self.highscores_list_display = ""
		self.get_highscores_list()

		self.animation_folder = ".\\image\\classic_gif\\"
		self.animation_counter = 1
		self.animation_speed = 400
		self.max_animation_frames = 14

		pygame.time.set_timer(pygame.USEREVENT + 1, config.get_init_delay())		# Set a new event to appear every 1 second (1000 milisecond)
		pygame.time.set_timer(pygame.USEREVENT + 2, animation_speed)
		if (config.is_sound()):
			self.PlayIngameTheme()
		self.PlayEndGameTheme = False #check if the theme song when gameover have been played or not	

	# Display message start at topleft = (x, y) coordinate
	def disp_msg(self, msg, topleft, mode = 0):
		x, y = topleft
		for line in msg.splitlines():
			# Write text in a blit surface start with (x, y) location
			if (mode == 1):
				self.screen.blit(
					self.default_font_number.render(line, False, config.get_text_color(), config.get_background_color()),
					(x,y))
			else:
				self.screen.blit(
					self.default_font.render(line, False, config.get_text_color(), config.get_background_color()),
					(x,y))
			y = y + config.get_font_size() + 2
	
	# Display message at the center of the screen
	def center_msg(self, msg):
		for i, line in enumerate(msg.splitlines()):
			msg_image =  self.default_font.render(line, False, config.get_text_color(), config.get_background_color())
		
			msgim_center_x, msgim_center_y = msg_image.get_size()
			msgim_center_x //= 2
			msgim_center_y //= 2
		
			self.screen.blit(msg_image, (self.width // 2 - msgim_center_x, self.height // 2 - msgim_center_y + i * (config.get_font_size() + 10)))
	
	def draw_matrix(self, matrix, offset):
		off_x, off_y  = offset
		for y, row in enumerate(matrix):
			for x, val in enumerate(row):
				if val:
					pygame.draw.rect(self.screen, config.get_color(val), pygame.Rect(
																				(off_x + x) * cell_size,
																				(off_y + y) * cell_size, 
																				cell_size, cell_size),	0)

	def generate_stone_location(self):
		res = []        # Store x, y location of the stone
		res.append(int(cols / 2 - len(self.player_1.stone[0]) / 2))
		res.append(0)
		return res

	def new_stone(self):
		self.player_1.stone = self.player_1.next_stone[:]			# Set current stone = next stone
		self.player_1.next_stone = block.get_random_tetris_shape()	# Generate new stone
		self.player_1.stone_x, self.player_1.stone_y = self.generate_stone_location()
		
		if check_collision(self.player_1.board, self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y)):	# The newly created stone collide with the others
			self.gameover = True
		
	def rotate_stone(self):
		if not self.gameover and not self.paused:
			new_stone = rotate_clockwise(self.player_1.stone)
			if not check_collision(self.player_1.board, new_stone, (self.player_1.stone_x, self.player_1.stone_y)):
				self.player_1.stone = new_stone

	# Update lines, level & point based on completed lines
	def add_cl_lines(self, completed_lines):
		self.player_1.lines += completed_lines
		self.player_1.score += config.get_linescores(completed_lines) * self.player_1.level

		if self.player_1.lines >= self.player_1.level * config.get_lines_per_level():
			self.player_1.level += 1
			# Set new delay based on level
			config.set_delay(self.player_1.level)
			pygame.time.set_timer(pygame.USEREVENT + 1, config.get_delay())

	def clear_complete_rows(self):
		cleared_rows = 0
		while True:
			for i, row in enumerate(self.player_1.board[:-1]):
				if 0 not in row:
					self.player_1.board = remove_row(self.player_1.board, i, cols)
					cleared_rows += 1
					break
			else:
				break
		return cleared_rows
	
	# Move the block to the left/right (delta_x = -1/+1)
	def move(self, delta_x):
		if not self.gameover and not self.paused:
			new_x = self.player_1.stone_x + delta_x			# Change x-coordinate of the block
			# Check for collision & contraint
			if new_x < 0:
				new_x = 0
			if new_x > cols - len(self.player_1.stone[0]):
				new_x = cols - len(self.player_1.stone[0])
			if not check_collision(self.player_1.board, self.player_1.stone, (new_x, self.player_1.stone_y)):
				self.player_1.stone_x = new_x
	
	def drop(self, manual):
		if not self.gameover and not self.paused:
			self.player_1.score += 0 if manual else 0	# If fast drop, more point (no used so set = 0)
			self.player_1.stone_y += 1
			if check_collision(self.player_1.board, self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y)):
				self.player_1.board = join_matrixes(self.player_1.board, self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y))
				self.new_stone()
				cleared_rows = self.clear_complete_rows()
				self.add_cl_lines(cleared_rows)
				return True
		return False
	
	def instant_drop(self):
		if not self.gameover and not self.paused:
			while(not self.drop(True)):
				pass

	def get_highscores_list(self):
		self.highscores_list = []
		self.highscores_list_display = "\nHIGHSCORES:\n"
		if os.path.isfile("highscores.txt") == True:
			file_reader_highscores = open("highscores.txt","r")
			for idx in range(0,5):
				self.highscores_list.append(str(file_reader_highscores.readline()))
			file_reader_highscores.close();
		else:
			self.highscores_list = config.get_default_highscores()
		self.highscores_list_display += ''.join(score for score in self.highscores_list)
	
	def start_game(self):
		if self.gameover:
			self.init_game()
			self.gameover = False

	def quit(self):
		self.center_msg("Exiting.......")
		pygame.display.update()
		sys.exit()

	def toggle_pause(self):
		self.paused = not self.paused
	
	def print_game_over(self):
		file_writer_highscores = open("highscores.txt","w")
		#check if current player's score is new highscore
		new_highscore = False		
		msg = ""
		for highscore in self.highscores_list:
			if self.player_1.score > int(highscore) and new_highscore == False:
				highscore = str(self.player_1.score) + "\n"
				new_highscore = True
				msg += str(self.player_1.score) + " (NEW)\n"
			else:
				msg += highscore
			file_writer_highscores.write(highscore)
		file_writer_highscores.close()
		if new_highscore == True:
			self.center_msg("""	Game Over!\nYour score: %d\nNEW HIGHSCORE\nHIGHSCORE\n%s\nPress space to continue""" % (self.player_1.score, msg))
			#play sound when player have new highscore
			if self.PlayEndGameTheme == False and config.is_sound():
				config.PlayThemeSong("Theme\New_Highscore.mp3",0.05,0)
				self.PlayEndGameTheme = True
		else:
			self.center_msg("""	Game Over!\nYour score: %d\nHIGHSCORE\n%s\nPress space to continue""" % (self.player_1.score, msg))
			#play sound when game over
			if self.PlayEndGameTheme == False and config.is_sound():
				config.PlayThemeSong("Theme\Game_Over.mp3",0.0,0)
				self.PlayEndGameTheme = True

	def print_pause(self):
		self.center_msg("Paused")

	def print_info(self):
		pygame.draw.line(self.screen, config.get_text_color(), (self.rlim + 1, 0), (self.rlim + 1, self.height - 1))

		self.disp_msg("Tetris - Classic", (self.rlim + cell_size * 0.5 , cell_size / 2))
		self.disp_msg("Next:", ( self.rlim + cell_size, cell_size * 2))
		self.disp_msg("Score: ", (self.rlim + cell_size, cell_size * 6))
		self.disp_msg("%d\n\n" % (self.player_1.score), (self.rlim + cell_size * 1.5, cell_size * 7), 1)
		self.disp_msg("Level: ", (self.rlim + cell_size, cell_size * 9))
		self.disp_msg("%d\n" % (self.player_1.level), (self.rlim + cell_size * 3, cell_size * 10), 1)
		self.disp_msg("Lines: ", (self.rlim + cell_size , cell_size * 12))
		self.disp_msg("%d" % (self.player_1.lines), (self.rlim + cell_size * 3, cell_size * 13), 1)

		self.draw_matrix(self.bground_grid, (0,0))
		self.draw_matrix(self.player_1.board, (0,0))
		self.draw_matrix(self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y))

		self.draw_matrix(self.player_1.next_stone, (cols + 1, 3))

		self.disp_msg(self.highscores_list_display,(self.rlim + cell_size, cell_size * 15))
		self.update_animation()

	def update_animation(self):
		image = pygame.image.load(self.animation_folder + 'image (' + str(self.animation_counter) + ').gif')
		self.screen.blit(image, [self.width - 125, self.height - 130])

		pygame.display.flip()

	def excecute_event(self, key_actions):
		for event in pygame.event.get():
			if event.type == pygame.USEREVENT + 1:
				self.drop(False)
			elif event.type == pygame.USEREVENT + 2:
				self.animation_counter += 1
				if (self.animation_counter > self.max_animation_frames):
					self.animation_counter = 1
			elif event.type == pygame.QUIT:
				self.quit()
			elif event.type == pygame.KEYDOWN:
				for key in key_actions:
					if event.key == eval("pygame.K_" + key):
						key_actions[key]()

	def update_status_on_screen(self):
		self.screen.fill(config.get_background_color())
		if self.gameover:
			self.print_game_over()
		else:
			if self.paused:
				self.print_pause()
			else:
				self.print_info()
		pygame.display.update()

	def PlayIngameTheme(self):
		#play theme song while playing
		config.PlayThemeSong("Theme\Tetris_Classic.mp3",0.0,-1)

	def run(self):

		key_actions = {
			config.get_key_quit()			:	self.quit,
			config.get_key_left()			:	lambda:self.move(-1),
			config.get_key_right()			:	lambda:self.move(+1),
			config.get_key_down()			:	lambda:self.drop(True),
			config.get_key_up()				:	self.rotate_stone,
			config.get_key_pause()			:	self.toggle_pause,
			config.get_key_start()			:	self.start_game,
			config.get_key_instant_drop()	:	self.instant_drop
		}
		
		self.gameover = False
		self.paused = False
		
		dont_burn_my_cpu = pygame.time.Clock()
		while 1:
			self.update_status_on_screen()
			self.excecute_event(key_actions)
			dont_burn_my_cpu.tick(maxfps)

if __name__ == '__main__':
	App = TetrisApp_Mode_1Player()
	App.run()
