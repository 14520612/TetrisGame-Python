import pygame
from random import randrange as rand

# The configuration
class DefaultConfig:
	__font = [	".\\Font\\computer_pixel_7\\computer_pixel-7.ttf",
				".\\Font\\vermin_vibes_1989\\vermin_vibes_1989.ttf", 
				".\\Font\\pixel_lcd7\\PixelLCD-7.ttf",
				".\\Font\\style-7_bubble-pixel-7\\bubble_pixel-7.ttf",
				".\\Font\\opipik_sanstaina\\sanstaina.ttf",
				".\\Font\\luedecke-design-fonts_pxlvetica\\Pxlvetica.ttf",
				".\\Font\\style-7_thin-pixel-7\\thin_pixel-7.ttf",
				".\\Font\\cumberland-fontworks_merchant-copy\\MerchantCopy.ttf", 
				".\\Font\\freaky-fonts_gamegirl-classic\\Gamegirl.ttf",
				".\\Font\\minecraftia\\Minecraftia-Regular.ttf",
				".\\Font\\petyka_retro_computer_short\\Petyka-RetroComputer___SHORT.ttf",
				".\\Font\\bloodfsw\\bloofs__.ttf",	
				".\\Font\\arcade\\ARCADE.TTF",
				".\\Font\\tetricide-brk\\tetri.ttf", 
				".\\Font\\digital-7\\digital-7 (mono).ttf"]

	# Main menu config
	__menu_background_color = (0, 0, 0)
	__menu_text_color = (255, 255, 255)
	__menu_selector_color = (255, 0, 0)
	__menu_normal_font_size = 25
	__menu_title_font_size = 45
	__menu_title = "TETRIS THE GAME"
	__menu_width = 450
	__menu_height = 650
	__menu_choices = ["Classic Mode", "Multiplayer Mode", "Ghost Mode", "Arcade Mode", "Info"]
	__menu_choices_description = [	"Enjoy the classical Tetris Game from your childhood",
								 	"Ask your friends for a solo in Tetris Game", 
								 	"Challenge yourself with our custom Tetris Game", "where you can't see blocks",
								 	"A classic Tetris Game with many custom features ",
								 	"    Ultimate Arrows which destroys any columns",
								 	"    Randrops which drop blocks anywhere on the map"]
	__menu_choices_distance = 80

	# Game Experiment Config
	__cell_size =	25	# The size of each cell
	__cols =		15	# Number of columns (Tetris Board)
	__rows =		25	# Number of rows (Tetris Board)
	__maxfps = 		30	# Maximum frame per second
	__info_width = 7	# The width of the right-size infomation board

	# Game Difficulty
	__default_level = 1
	__lines_per_level = 6 # The lines needed to complete before increasing level
	__linescores = [0, 40, 100, 300, 1200, 6000]
	__init_delay = 1000
	__delay = __init_delay
	__default_highscores = ["5000\n","3000\n","1500\n","1000\n","500\n"]

	# 2-player mode
	__multiplayer_const_delay = 500
	__multiplayer_max_point = 10000

	# Arcade mode
	__blocks_per_randrop = 10
	__arrow_chance = 10
        
	
	# Helper color for background grid
	__colors = [
				(0,   0,   0  ),
				(255, 85,  85),
				(100, 200, 115),
				(120, 108, 245),
				(255, 140, 50 ),
				(50,  120, 52 ),
				(146, 202, 73 ),
				(150, 161, 218 ),
				(20, 20, 20) 
				]

	# Game Font & Screen Color
	__text_color = (255, 255, 255)
	__background_color = (65, 72, 84)
	__font_size = 14

	# Game Basic Key
	__key_quit = 'ESCAPE'
	__key_pause = 'p'
	__key_start = 'SPACE'

	__sound = True



	# Getter function
	def is_sound(self):
		return self.__sound

	def get_font(self, index):
		if (index < 0 or index > len(self.__font)):
			index = 0
		return self.__font[index]

	def get_menu_selector_color(self):
		return self.__menu_selector_color

	def get_menu_choices_distance(self):
		return self.__menu_choices_distance

	def get_menu_choices(self, index):
		if (index < 0 or index >= len(self.__menu_choices)):
			return ""
		else:
			return self.__menu_choices[index]

	def get_menu_choices_description(self, index):
		if (index < 0 or index >= len(self.__menu_choices_description)):
			return "No Guide Found"
		else:
			return self.__menu_choices_description[index]

	def get_menu_choices_num(self):
		return len(self.__menu_choices)

	def get_menu_width(self):
		return self.__menu_width

	def get_menu_height(self):
		return self.__menu_height

	def get_menu_title(self):
		return self.__menu_title

	def get_menu_title_font_size(self):
		return self.__menu_title_font_size

	def is_arrow_chance(self):
		res = rand(100)
		if(res <= self.__arrow_chance):
			return True
		else:
			return False

	def get_blocks_per_randrop(self):
		return self.__blocks_per_randrop
		
	def get_cell_size(self):
		return self.__cell_size

	def get_cols(self):
		return self.__cols

	def get_rows(self):
		return self.__rows

	def get_maxfps(self):
		return self.__maxfps

	def get_info_width(self):
		return self.__info_width

	def get_color(self, index):
		return self.__colors[index]

	def get_font_size(self):
		return self.__font_size

	def get_default_level(self):
		return self.__default_level

	def get_text_color(self):
		return self.__text_color

	def get_background_color(self):
		return self.__background_color

	def get_key_quit(self):
		return self.__key_quit

	def get_key_start(self):
		return self.__key_start

	def get_key_pause(self):
		return self.__key_pause

	def get_lines_per_level(self):
		return self.__lines_per_level

	def get_linescores(self, index):
		if index < 0:
			return self.__linescores[0]
		if index >= len(self.__linescores):
			return self.__linescores[len(self.__linescores) - 1]
		return self.__linescores[index]

	def get_delay(self):
		return self.__delay

	def get_init_delay(self):
		return self.__init_delay

	def set_delay(self, level):
		newdelay = self.__init_delay - 200 * (level - 1)
		newdelay = 100 if newdelay < 100 else newdelay
		self.__delay = newdelay

	def get_multiplayer_const_delay(self):
		return self.__multiplayer_const_delay

	def get_default_highscores(self):
		return self.__default_highscores

	def get_multiplayer_max_point(self):
		return self.__multiplayer_max_point

	def PlayThemeSong(self,file_path,start_from,loop):
		pygame.mixer.music.load(str(file_path))
		pygame.mixer.music.play(int(loop),float(start_from))

	def get_menu_background_color(self):
		return self.__menu_background_color

	def get_menu_text_color(self):
		return self.__menu_text_color

	def get_menu_normal_font_size(self):
		return self.__menu_normal_font_size

# Key configuration 1
class DefaultConfig_Player_1(DefaultConfig):
	__key_up = 'w'
	__key_down = 's'
	__key_left = 'a'
	__key_right = 'd'
	__key_instant_drop = 'TAB'

	def get_key_up(self):
		return self.__key_up

	def get_key_down(self):
		return self.__key_down

	def get_key_left(self):
		return self.__key_left

	def get_key_right(self):
		return self.__key_right

	def get_key_instant_drop(self):
		return self.__key_instant_drop

# Key configuration 2
class DefaultConfig_Player_2(DefaultConfig):
	__key_up = 'UP'
	__key_down = 'DOWN'
	__key_left = 'LEFT'
	__key_right = 'RIGHT'
	__key_instant_drop = 'RETURN'

	def get_key_up(self):
		return self.__key_up

	def get_key_down(self):
		return self.__key_down

	def get_key_left(self):
		return self.__key_left

	def get_key_right(self):
		return self.__key_right

	def get_key_instant_drop(self):
		return self.__key_instant_drop
