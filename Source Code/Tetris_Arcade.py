import pygame, sys, os.path
from Tetris_Config import *
from Tetris_Blocks import *
from Tetris_Boards import *
from Tetris_Player import *
from Tetris_Classic import *

config = DefaultConfig_Player_2()
block = Blocks()
cell_size =	config.get_cell_size()
cols =		config.get_cols()
rows =		config.get_rows()
maxfps = 	config.get_maxfps()

class TetrisApp_Mode_Arcade(TetrisApp_Mode_1Player):
	def init_game(self):
		self.player_1 = TetrisPlayer()
		self.player_1.board = new_board(rows, cols)
		self.player_1.next_stone = block.get_random_tetris_shape()	# Generate a new stone (block) randomly

		self.blocks_counter = 0			# count the num of blocks
		self.new_stone()

		self.is_instant_drop = False
		self.is_arrow = False
		self.highscores_list = []
		self.highscores_list_display = ""
		self.get_highscores_list()

		self.animation_folder = ".\\image\\arcade_gif\\"
		self.animation_counter = 1
		self.animation_speed = 400
		self.max_animation_frames = 50
		
		pygame.time.set_timer(pygame.USEREVENT + 1, config.get_init_delay())		# Set a new event to appear every 1 second (1000 milisecond)
		pygame.time.set_timer(pygame.USEREVENT + 2, animation_speed)

		if (config.is_sound()):
			self.PlayIngameTheme()
		self.PlayEndGameTheme = False

	def PlayIngameTheme(self):
		#play theme song while playing
		config.PlayThemeSong("Theme\Tetris_Arcade.mp3",1.0,-1)
	
	def generate_stone_location(self):
		res = []        # Store x, y location of the stone        
		if (self.blocks_counter % config.get_blocks_per_randrop() == 0):
			res.append(rand(cols - len(self.player_1.stone[0])))
			self.is_instant_drop = True
		else:
			res.append(int(cols / 2 - len(self.player_1.stone[0]) / 2))
		res.append(0)
		return res
		      
	def new_stone(self):
		self.player_1.stone = self.player_1.next_stone[:]			# Set current stone = next stone
		if (config.is_arrow_chance()):
                        self.player_1.next_stone = block.get_tetris_arrow()
                        
                else:
                        self.player_1.next_stone = block.get_random_tetris_shape()      # Generate new stone
                if (self.player_1.stone == block.get_tetris_arrow()):
                        self.is_arrow = True
	
		self.blocks_counter += 1
		self.player_1.stone_x, self.player_1.stone_y = self.generate_stone_location()
		if check_collision(self.player_1.board, self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y)):	# The newly created stone collide with the others
			self.gameover = True
                

	def drop(self, manual):
		if not self.gameover and not self.paused:
			self.player_1.score += 0 if manual else 0	# If fast drop, more point
			self.player_1.stone_y += 1
			if check_collision(self.player_1.board, self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y)):
				self.player_1.board = join_matrixes(self.player_1.board, self.player_1.stone, (self.player_1.stone_x, self.player_1.stone_y))
				if self.is_arrow == True:
					remove_col(self.player_1.board, self.player_1.stone_x)
					self.is_arrow = False
				self.new_stone()
				cleared_rows = self.clear_complete_rows()
				self.add_cl_lines(cleared_rows)
				return True
		return False

	def rotate_stone(self):
		if not self.gameover and not self.paused:
			new_stone = rotate_clockwise(self.player_1.stone)
			if not check_collision(self.player_1.board, new_stone, (self.player_1.stone_x, self.player_1.stone_y)):
				self.player_1.stone = new_stone

	def print_info(self):
		TetrisApp_Mode_1Player.print_info(self)
		self.disp_msg("Tetris - Arcade ", (self.rlim + cell_size * 0.5 , cell_size / 2))

		if ((self.blocks_counter + 1) % config.get_blocks_per_randrop() == 0):
			self.disp_msg("Warning Randrop\n", (self.rlim + cell_size / 1.5, cell_size * 20))

	def update_animation(self):
		image = pygame.image.load(self.animation_folder + 'image (' + str(self.animation_counter) + ').gif')
		self.screen.blit(image, [self.width - 145, self.height - 120])

		pygame.display.flip()

	def instant_drop(self):
		if not self.gameover and not self.paused:
			while(not self.drop(True)):
				if self.is_instant_drop == True:
					pygame.time.wait(10)
					self.update_status_on_screen()
				else:
					pass
               
	def excecute_event(self, key_actions):
		for event in pygame.event.get():
			if event.type == pygame.USEREVENT + 1:
				if (self.is_instant_drop):
					self.instant_drop()
					self.is_instant_drop = False
				else:
					self.drop(False)
			elif event.type == pygame.USEREVENT + 2:
				self.animation_counter += 1
				if (self.animation_counter > self.max_animation_frames):
					self.animation_counter = 1
			elif event.type == pygame.QUIT:
				self.quit()
			elif event.type == pygame.KEYDOWN:
				for key in key_actions:
					if event.key == eval("pygame.K_" + key):
						key_actions[key]()

	def update_status_on_screen(self):
		self.screen.fill(config.get_background_color())
		if self.gameover:
			self.print_game_over()
		else:
			if self.paused:
				self.print_pause()
			else:
				self.print_info()
		pygame.display.update()
             
	def run(self):
		key_actions = {
			config.get_key_quit()			:	self.quit,
			config.get_key_left()			:	lambda:self.move(-1),
			config.get_key_right()			:	lambda:self.move(+1),
			config.get_key_down()			:	lambda:self.drop(True),
			config.get_key_up()			:	self.rotate_stone,
			config.get_key_pause()			:	self.toggle_pause,
			config.get_key_start()			:	self.start_game,
			config.get_key_instant_drop()	        :	self.instant_drop
		}
		
		self.gameover = False
		self.paused = False
		
		dont_burn_my_cpu = pygame.time.Clock()
		while 1:
			self.update_status_on_screen()
			self.excecute_event(key_actions)
			dont_burn_my_cpu.tick(maxfps)

if __name__ == '__main__':
	App = TetrisApp_Mode_Arcade()
	App.run()