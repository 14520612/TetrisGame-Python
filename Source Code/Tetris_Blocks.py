from random import randrange as rand

class Blocks:
	# Define the shapes of the single parts
	__tetris_shapes = [
						[	[1, 1, 1],
	 						[0, 1, 0]	],
	
						[	[0, 2, 2],
	 						[2, 2, 0]	],
	
						[	[3, 3, 0],
	 						[0, 3, 3]	],
	
						[	[4, 0, 0],
	 						[4, 4, 4]	],
	
						[	[0, 0, 5],
	 						[5, 5, 5]	],
	
						[	[6, 6, 6, 6]],
	
						[	[7, 7],
	 						[7, 7]		]
					]

	__tetris_arrow = [[      [7]       ]]

	def get_tetris_arrow(self):
		return self.__tetris_arrow[0]
        
	def get_tetris_shape(self, index):
		return self.__tetris_shapes[index]

	def get_random_tetris_shape(self):
		return self.get_tetris_shape(rand(len(self.__tetris_shapes)))



def rotate_clockwise(shape):
	return [ [ shape[y][x]
		for y in xrange(len(shape)) ]
	for x in xrange(len(shape[0]) - 1, -1, -1) ]
